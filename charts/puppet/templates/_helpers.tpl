{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "puppet.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "puppet.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "puppet.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "puppet.ca.fullname" -}}
{{ template "puppet.fullname" . }}-{{ .Values.ca.name }}
{{- end -}}

{{- define "puppet.compiler.fullname" -}}
{{ template "puppet.fullname" . }}-{{ .Values.compiler.name }}
{{- end -}}

{{- define "puppet.puppetdb.fullname" -}}
{{ template "puppet.fullname" . }}-{{ .Values.puppetdb.name }}
{{- end -}}

{{- define "puppet.postgresql.fullname" -}}
{{ template "puppet.fullname" . }}-{{ .Values.postgresql.name}}
{{- end -}}

{{- define "puppet.puppetboard.fullname" -}}
{{ template "puppet.fullname" . }}-{{ .Values.puppetboard.name }}
{{- end -}}